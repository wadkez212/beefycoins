<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать файл в "wp-config.php"
 * и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки базы данных
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://ru.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Параметры базы данных: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'coins' );

/** Имя пользователя базы данных */
define( 'DB_USER', 'root' );

/** Пароль к базе данных */
define( 'DB_PASSWORD', '123' );

/** Имя сервера базы данных */
define( 'DB_HOST', '127.0.0.1' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );
define('FS_METHOD', 'direct');






/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу. Можно сгенерировать их с помощью
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}.
 *
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными.
 * Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'xB(rTOX*]>ms:.Wk jX|KKT&-8BD%xuc/K`a4MT~.p[UpZ8 r2~mZOxHkhj0eC#5' );
define( 'SECURE_AUTH_KEY',  'CP9 B{]IK>n$seWyHHlP?yccPgF6#yOg5 ju_Im,7~R3(|~NCf+6kx |]?8Wl5g9' );
define( 'LOGGED_IN_KEY',    '5(D>KF4/tWw5d(=I]rw5(_NX`Srp>rrw5P0=CG&/TvmF)hFl~kCQHOla=lKNX_s&' );
define( 'NONCE_KEY',        '.F1YZ9|!tS]BE`j?xlatRl2Gc@XjVRepRN*!0Zd,Cix0<sT1%1z`_#(>6o8C,b]~' );
define( 'AUTH_SALT',        'TibyKprdKa[8g((NnHB;qlqg!~J=I#V$m<[]XJPcN(q`0`rVa)0.N>U`Um:8&]YA' );
define( 'SECURE_AUTH_SALT', 'apGJQ}{QE;O.wxk~GYo:5HYX>aaF7[ahO:%9DV2RK;<r!5.BN:W?>:iYz{}s;7rW' );
define( 'LOGGED_IN_SALT',   '%T2%2af}f=}_xeO}ZJ]m5[:ZPn%U&Wp6~x%S #.ThHQ=-%hH5w}YK)wJd:oeeCb]' );
define( 'NONCE_SALT',       'dm(c .j1OIoj:Sy/8F}5mjZTT/~R~{^6_H|?siIrmNt[Xwee O;f>sjav-% 3O[n' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в документации.
 *
 * @link https://ru.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Произвольные значения добавляйте между этой строкой и надписью "дальше не редактируем". */



/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once ABSPATH . 'wp-settings.php';
