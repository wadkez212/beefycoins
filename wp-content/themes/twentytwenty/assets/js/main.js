
$(document).ready(function(){
    // для плавного скролла по якорям
  

    $('.navbar').click(function(){
        if($('.navbar-toggler').attr('aria-expanded') == 'true'){
            $('.icon_top_head_1').css('display','none');
            $('.icon_top_head_2').css('display','block');
        }else{
            $('.icon_top_head_1').css('display','block');
            $('.icon_top_head_2').css('display','none');
        }
    })
    var swiper = new Swiper(".mySwiper", {
        loop: true,
        navigation: {
            nextEl: ".slider_btn_next1",
            prevEl: ".slider_btn_prev1",
        },
        pagination: {
            el: ".swiper-pagination",
        },
    });


    var count = 4;

    var ww = jQuery(window).width();
    if (ww <= 1100) count = 3;
    if (ww <= 768) count = 2;


    var swiper = new Swiper(".swiper_combo", {
        slidesPerView: count,
        spaceBetween: 30,
        slidesPerGroup: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        pagination: {
        el: ".swiper-pagination",
        clickable: true,
        },
        navigation: {
        nextEl: ".combo_slider_btn_next",
        prevEl: ".combo_slider_btn_prev",
        },
    });

    var swiper = new Swiper(".swiper_shaurma", {
        slidesPerView: count,
        spaceBetween: 30,
        slidesPerGroup: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        pagination: {
        el: ".swiper-pagination",
        clickable: true,
        },
        navigation: {
        nextEl: ".shaurma_slider_btn_next",
        prevEl: ".shaurma_slider_btn_prev",
        },
    });

    var swiper = new Swiper(".swiper_doner", {
        slidesPerView: count,
        spaceBetween: 30,
        slidesPerGroup: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        pagination: {
        el: ".swiper-pagination",
        clickable: true,
        },
        navigation: {
        nextEl: ".doner_slider_btn_next",
        prevEl: ".doner_slider_btn_prev",
        },
    });

    var swiper = new Swiper(".swiper_mangal", {
        slidesPerView: count,
        spaceBetween: 30,
        slidesPerGroup: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        pagination: {
        el: ".swiper-pagination",
        clickable: true,
        },
        navigation: {
        nextEl: ".mangal_slider_btn_next",
        prevEl: ".mangal_slider_btn_prev",
        },
    });


    var swiper = new Swiper(".swiper_salat", {
        slidesPerView: count,
        spaceBetween: 30,
        slidesPerGroup: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        pagination: {
        el: ".swiper-pagination",
        clickable: true,
        },
        navigation: {
        nextEl: ".salat_slider_btn_next",
        prevEl: ".salat_slider_btn_prev",
        },
    });

    var swiper = new Swiper(".swiper_sup", {
        slidesPerView: count,
        spaceBetween: 30,
        slidesPerGroup: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        pagination: {
        el: ".swiper-pagination",
        clickable: true,
        },
        navigation: {
        nextEl: ".sup_slider_btn_next",
        prevEl: ".sup_slider_btn_prev",
        },
    });

    var swiper = new Swiper(".swiper_snacks_sous", {
        slidesPerView: count,
        spaceBetween: 30,
        slidesPerGroup: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        pagination: {
        el: ".swiper-pagination",
        clickable: true,
        },
        navigation: {
        nextEl: ".snacks_sous_slider_btn_next",
        prevEl: ".snacks_sous_slider_btn_prev",
        },
    });

    var swiper = new Swiper(".swiper_water", {
        slidesPerView: count,
        spaceBetween: 30,
        slidesPerGroup: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        pagination: {
        el: ".swiper-pagination",
        clickable: true,
        },
        navigation: {
        nextEl: ".water_slider_btn_next",
        prevEl: ".water_slider_btn_prev",
        },
    });

    var swiper = new Swiper(".swiper_pahlava", {
        slidesPerView: count,
        spaceBetween: 30,
        slidesPerGroup: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        pagination: {
        el: ".swiper-pagination",
        clickable: true,
        },
        navigation: {
        nextEl: ".pahlava_slider_btn_next",
        prevEl: ".pahlava_slider_btn_prev",
        },
    });


    let scroll = new SmoothScroll('a[href*="#"]', {
        speed: 500,
       });
    





    //Меню - активный класс
    $(".categories_title").on("click", function() {

 
            $(".categories_title").each(function (index, element) {     //перебераем все классы categorie title

                //console.log('Индекс элемента: ' + index + '; class элемента = ' + $(element).attr('class')); 
                $(element).removeClass('active');
                
            });

            $(this).addClass("active");
        
    });


//     function menuItemToggle() {
//         const menuItems = document.querySelectorAll('.categories_title');

//         menuItems.forEach(element => {
//             element.addEventListener('click', function(){
//                 menuItems.forEach(element => {
//                     element.classList.remove('active'); 
//                 });
//                 this.classList.add('active');
//             });
//         });
//     }
//     menuItemToggle();
 });
