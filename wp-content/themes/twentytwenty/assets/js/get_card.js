jQuery(document).on('click','.card',function(){
    var card_id =  jQuery(this).attr('data');
    
    jQuery.ajax({
     url: window.wp_data.ajax_url,
     dataType: "html",
     cache: false,
     data: {
         action : 'get_card',
         card_id: card_id
     },
     success: function(data) {

        var data = JSON.parse(data);
        //console.log(data.post.post_title);
        
        $('.modal_image').attr('src', data.image_src);
        $('.generated_modal').find('.modal_title').html(data.post.post_title);
        $('.generated_modal').find('.modal_content').html(data.post.post_content);
        //$('.generated_modal').find('.modal_price').html(data.price + ' Р');
        //$('.generated_modal').find('.modal_weight').html(data.weight + ' Г');
        
        //console.log(data);


        // window.location.href= "/";
     }
 });
 
 });


 function test(e){
    
    const cardPopup = document.querySelector('.popup_content'); // получаем к классу где будем размещать эл-ты в popup
    const cardProps = e.querySelectorAll('.card_props');    //получаем доступ к блоку где расположено price и weight

    cardPopup.innerHTML = "";    //очищаем popup

    const modalImg = document.querySelector('.modal_image'); //картинка

    const cardImg = e.querySelector('img').src;
    modalImg.src = cardImg;

    cardProps.forEach(element => {
        //извлекаем вес и цену
        let itemDscrBoxPrice = element.querySelector('.price').textContent;
        let itemDscrBoxWeight = element.querySelector('.weight').textContent;

        //создаем блок который будем вставлятьт в popup
        let innerHtmlCart = `
        <div class="block_modal">
            <div class="modal_price">${itemDscrBoxPrice}</div>
            <div class="modal_weight">${itemDscrBoxWeight}</div>
        </div>
        `;

        cardPopup.insertAdjacentHTML('beforeend', innerHtmlCart);  // добавляем текст в popup
        //console.log(innerHtmlCart);

    });
    
}







 

