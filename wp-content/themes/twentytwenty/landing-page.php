<?php
/* Template Name: Landing-page */
session_start();
?>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <?php wp_head(); ?>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
     <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="<?php echo get_site_url(); ?>/wp-content/themes/twentytwenty/assets/js/get_card.js"></script>
    <script src="<?php echo get_site_url(); ?>/wp-content/themes/twentytwenty/assets/js/polyfills.js"></script>
    <script src="<?php echo get_site_url(); ?>/wp-content/themes/twentytwenty/assets/js/main.js"></script>
    


    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css"
    />
    <link href="http://fonts.cdnfonts.com/css/bebas-neue" rel="stylesheet">


</head>
<header>
    <div class="container">
        <div class="row block_1">
            <div class="col-6 col-lg-3 logo_col"><img src="<?php echo get_template_directory_uri()?>/assets/images/Logo.png">
                <div class="text-time d-none d-sm-block">Ежедневно 10:00 - 22:00</div>
            </div>
            <div class="col-3 d-none d-sm-flex"><?php wp_nav_menu() ?></div>
            <div class="col-3 d-none d-sm-flex ikonki">

            <!-- POPup  верхний -->
            <div class="rabota" data-toggle="modal" data-target=".bd-example-modal-lg"></div>
                <img src="<?php echo get_template_directory_uri()?>/assets/images/icon_shava.png">
                <img src="<?php echo get_template_directory_uri()?>/assets/images/insta.svg">
                <img src="<?php echo get_template_directory_uri()?>/assets/images/vk.svg"> </div>
            <div class="col-3 d-none d-sm-block telefon"><p class="nomer">+7 (951) 996-22-99</p><p class="text">Отдел контроля качества</p></div>
            
            <div class="col-6  d-sm-block d-md-none d-lg-none">
                <nav class="navbar">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span><img class="icon_top_head_1" src="<?php echo get_template_directory_uri(); ?>/assets/images/burger.png"></span>
                        <span><img class="icon_top_head_2" style="display:none;" src="<?php echo get_template_directory_uri(); ?>/assets/images/krest.png"></span>
                    </button>
                </nav>
            </div>
        </div>
        <div class="">
            <div class="collapse" id="navbarToggleExternalContent">
                <div class="mobile_menu">
                    <h4 class="text-white"></h4>
                    <?php wp_nav_menu() ?>
                    <div class="corch">
                    <div class="ikonki">
                    <div class="rabota"></div>
                    <img src="<?php echo get_template_directory_uri()?>/assets/images/icon_shava.png">
                    <img src="<?php echo get_template_directory_uri()?>/assets/images/insta.svg">
                    <img src="<?php echo get_template_directory_uri()?>/assets/images/vk.svg">
                    </div>
                    <div class="text-time">Режим работы заведения:</div>
                    <div class="text-time">Ежедневно 10:00 - 22:00</div>
                    <div class="telefon"><p class="nomer">+7 (951) 996-22-99</p><p class="text">Отдел контроля качества</p></div>
                        <div class="text-time">ИНН 890412690088 <br>
                            ОГРН 321784700226547
                        </div>
                        <div class="text-time">© 2022 Beefy Coin</div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    
</header>
<section class="slider">
    <div class="container-fluid black">
        <div class="container">

    

            <div class="swiper_arrow slider_btn_prev1"><img src="<?php echo get_template_directory_uri()?>/assets/images/arrow_left.svg" alt=""></div>
            <div class="swiper_arrow slider_btn_next1"><img src="<?php echo get_template_directory_uri()?>/assets/images/arrow_right.svg" alt=""></div>
            

            <div class="swiper mySwiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="row slider_content">
                            <div class="col-sm-6 col-lg-6">
                                    <div class="o_nas_text">ВКУС НА ВЕС ЗОЛОТА</div>
                                    <p class="o_nas_litle">
                                        Наша задача - покорить сердца гостей незабываемым вкусом стритфуда с качественными ингредиентами как в ресторане и ингредиентами собственного производства по доступным ценам.
                                    </p>

                                    <div class="row">
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="slide1_newprice">1990 Р</div>

                                        </div>
                                        <div class="col-sm-12 col-lg-6">
                                            <div class="slide1_weight">за 1250 г</div>
                                            <div class="slide1_oldprice">2260 Р</div>
                                        </div>

                                        
                                    </div>
                            </div>

                            <div class="col-sm-6 col-lg-6"><img style="width: auto; height: auto;"src="<?php echo get_template_directory_uri()?>/assets/images/slide1.png">1</div>
                        </div>
                    </div>
                    <div class="swiper-slide">Slide 2</div>
                    <div class="swiper-slide">Slide 3</div>
                    <div class="swiper-slide">Slide 4</div>
                    <div class="swiper-slide">Slide 5</div>
                    <div class="swiper-slide">Slide 6</div>
                    <div class="swiper-slide">Slide 7</div>
                    <div class="swiper-slide">Slide 8</div>
                    <div class="swiper-slide">Slide 9</div>
                </div>
                <div class="swiper-pagination"></div>

            </div>
        </div>
    </div>
</section>


<div class="container-fluid black">
    <div class="container dostavka">
        <div class="row">
            <div class="col-lg-4 col-sm-12"><img src="<?php echo get_template_directory_uri()?>/assets/images/1.png"> </div>
            <div class="col-lg-4 col-sm-12"><img src="<?php echo get_template_directory_uri()?>/assets/images/2.png"></div>
            <div class="col-lg-4 col-sm-12"><img src="<?php echo get_template_directory_uri()?>/assets/images/3.png"></div>
        </div>
    </div>

    <div class="container o_nas">
        <div class="row">
            <div class="col-sm-12 col-lg-6">
                <img src="<?php echo get_template_directory_uri()?>/assets/images/o_nas.png">
             <div class="o_nas_text">
                 ВКУС НА ВЕС ЗОЛОТА
             </div>
                <p class="o_nas_litle">
                    Наша задача - покорить сердца гостей незабываемым вкусом стритфуда с качественными ингредиентами как в ресторане и ингредиентами собственного производства по доступным ценам.
                </p>


            </div>
            <div class="col-sm-12 col-lg-6 vidos"><video  width="540" height="300" autoplay="autoplay" controls="controls" poster="<?php echo get_template_directory_uri() ?>/assets/images/video_1.png"
                       name="Video Name" src="<?php echo get_template_directory_uri() ?>/assets/video/video.mov"></video></div>
        </div>
        <div class="row patterns d-sm-flex">
            <div class="col-lg-2 col-sm-4 col-6 about_block"><img src="<?php echo get_template_directory_uri()?>/assets/images/2.1.png"></div>
            <div class="col-lg-2 col-sm-4 col-6 about_block"><img src="<?php echo get_template_directory_uri()?>/assets/images/2.2.png"></div>
            <div class="col-lg-2 col-sm-4 col-6 about_block"><img src="<?php echo get_template_directory_uri()?>/assets/images/2.3.png"></div>
            <div class="col-lg-2 col-sm-4 col-6 about_block"><img src="<?php echo get_template_directory_uri()?>/assets/images/2.4.png"></div>
            <div class="col-lg-2 col-sm-4 col-6 about_block"><img src="<?php echo get_template_directory_uri()?>/assets/images/2.5.png"></div>
            <div class="col-lg-2 col-sm-4 col-6 about_block"><img src="<?php echo get_template_directory_uri()?>/assets/images/2.6.png"></div>
        </div>
    </div>
</div>


<div class="container-fluid grey_background">

    <!-- Menu and categories swap -->
    <div class="container">
        <h1 class="menu_header">Меню</h1>


        <div class="categories_wrapper d-flex justify-content-between">
            <a href="#combo" id="title_categories_combo" class="categories_title"><button class="menu_title">Комбо наборы</button></a>
            <a href="#shaurma" id="title_categories_shaurma" class="categories_title"><button class="menu_title">Шаурма</button></a>
            <a href="#doner" id="title_categories_doner" class="categories_title"><button class="menu_title">Донер</button></a>

            <a href="#mangal" id="title_categories_mangal" class="categories_title"><button class="menu_title">Мангал</button></a>

            <a href="#salat" id="title_categories_salat" class="categories_title"><button class="menu_title">Салаты</button></a>

            <a href="#sup" id="title_categories_sup" class="categories_title"><button class="menu_title">Супы</button></a>

            <a href="#water" id="title_categories_water" class="categories_title"><button class="menu_title">Напитки</button></a>
            <a href="#pahlava" id="title_categories_pahlava" class="categories_title"><button class="menu_title">Пахлава</button></a>


        </div>  




        <link
        rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.css"
        />
        <script src="https://cdn.jsdelivr.net/npm/swiper/swiper-bundle.min.js"></script>

    </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        
                        <div class="modal-body generated_modal">

                            <div class="row">
                                    <div class="col-md-6">
                                        <img class="modal_image" src="">
                                    </div>

                                    <div class="col-md-6">
                                        <div class="modal_title"></div>
                                            <div class="modal_content"></div>

                                            <div class="popup_content">
                                                 
                                            </div>
                                            
                                        </div>
                                    </div>
                                



                            </div>
                            <div class="modal-footer"></div>
                        </div>
                    </div>
        </div>


        <!-- Комбо наборы -->
    <section class="combo" id="combo">
        <div class="container-fluid">

            <div class="container">
                <div class="category_title">
                    <h1 class="category_title_header">Комбо наборы</h1>
                </div>

                <!-- Swiper Комбо наборы -->
                <div class="strelki">   

                        <div class="swiper_arrow slider_btn_prev combo_slider_btn_prev"><img src="<?php echo get_template_directory_uri()?>/assets/images/arrow_left.svg" alt=""></div>
                        <div class="swiper_arrow slider_btn_next combo_slider_btn_next"><img src="<?php echo get_template_directory_uri()?>/assets/images/arrow_right.svg" alt=""></div>

                    <div class="swiper swiper_categories swiper_combo">

                        <div class="swiper-wrapper">
                            <?php
                                global $post;
                                $postslist = get_posts( array('cat'=>'3' ) );

                                foreach ( $postslist as $post ){
                                    
                                    setup_postdata($post);
                                    
                            ?>

                            <div class="swiper-slide card item__poduct" data-toggle="modal" data-target="#exampleModalCenter" data=<?php echo the_ID() ?> onclick='test(this)'>

                                <?php the_post_thumbnail(); ?>

                                    <h2 class="header4 item__title"><?php the_title();?></h2>
                                    
                                    <div class="item__text">
                                        <?php the_content(); ?>
                                    </div>
                                    
                                    

                                    <div class="price__wrapper">
                                        <?php
                                            // проверяем есть ли в повторителе данные
                                            if( have_rows('price_weight_repeat') ):

                                                // перебираем данные
                                                while ( have_rows('price_weight_repeat') ) : the_row(); ?>
                                                <div class="card_props d-flex">

                                                    <div class="price_block">
                                                        <span class="price"> <?php the_sub_field('price'); ?> Р. </span>
                                                    </div>

                                                    <div class="weight_block">
                                                        <span class="weight"><?php the_sub_field('weight'); ?>  Г. </span>

                                                    </div> 
                                                    <?php
                                                        // отображаем вложенные поля
                                                        ?>
                                                </div>

                                                    <?php
                                                endwhile;

                                            else :

                                                // вложенных полей не найдено

                                            endif;

                                            ?>

                                        </div>
                                   

                            </div>
                            <?php
                                
                                }
                                wp_reset_postdata();  ?>
                                
                        </div>
                    </div>   
                </div> 
                        <!-- <div class="swiper-pagination"></div> -->
            </div>
        </div>
    </section>


    <section class="shaurma" id="shaurma">
        <div class="container-fluid">

            <div class="container">
                <div class="category_title">
                    <h1 class="category_title_header">Шаурма</h1>
                </div>

                <!-- Swiper Шаурма -->
                <div class="strelki">   

                    <div class="swiper_arrow slider_btn_prev shaurma_slider_btn_prev"><img style="width: 43px; height: 43px;"src="<?php echo get_template_directory_uri()?>/assets/images/arrow_left.svg" alt=""></div>
                    <div class="swiper_arrow slider_btn_next shaurma_slider_btn_next"><img style="width: 43px; height: 43px;"src="<?php echo get_template_directory_uri()?>/assets/images/arrow_right.svg" alt=""></div>

                    <div class="swiper swiper_categories swiper_shaurma">

                        <div class="swiper-wrapper">
                            <?php
                                global $post;
                                $postslist = get_posts( array('cat'=>'3' ) );

                                foreach ( $postslist as $post ){
                                    
                                    setup_postdata($post);
                                    
                            ?>

                            <div class="swiper-slide card" data-toggle="modal" data-target="#exampleModalCenter" data=<?php echo the_ID() ?>>

                                <?php the_post_thumbnail(); ?>

                                    <h2 class="header4"><?php the_title(); ?></h2>
                                    
                                    <?php the_excerpt(); ?>
                                    
                                    <div class="price__wrapper">
                                        <?php
                                            // проверяем есть ли в повторителе данные
                                            if( have_rows('price_weight_repeat') ):

                                                // перебираем данные
                                                while ( have_rows('price_weight_repeat') ) : the_row(); ?>
                                                <div class="card_props d-flex">

                                                    <div class="price_block">
                                                        <span class="price"> <?php the_sub_field('price'); ?> Р. </span>
                                                    </div>

                                                    <div class="weight_block">
                                                        <span class="weight"><?php the_sub_field('weight'); ?>  Г. </span>

                                                    </div> 
                                                    <?php
                                                        // отображаем вложенные поля
                                                        ?>
                                                </div>

                                                    <?php
                                                endwhile;

                                            else :

                                                // вложенных полей не найдено

                                            endif;

                                            ?>

                                        </div>

                            </div>
                            <?php
                                
                                }
                                wp_reset_postdata();  ?>
                                
                        </div>
                    </div>   
                </div> 
            </div>
        </div>
    </section>


    <section class="doner" id="doner">
        <div class="container-fluid">

            <div class="container">
                <div class="category_title">
                    <h1 class="category_title_header">Донер</h1>
                </div>

                <!-- Swiper Донер-->
                <div class="strelki">   

                        <div class="swiper_arrow slider_btn_prev doner_slider_btn_prev"><img src="<?php echo get_template_directory_uri()?>/assets/images/arrow_left.svg" alt=""></div>
                        <div class="swiper_arrow slider_btn_next doner_slider_btn_next"><img src="<?php echo get_template_directory_uri()?>/assets/images/arrow_right.svg" alt=""></div>

                    <div class="swiper swiper_categories swiper_doner">

                        <div class="swiper-wrapper">
                            <?php
                                global $post;
                                $postslist = get_posts( array('cat'=>'3' ) );

                                foreach ( $postslist as $post ){
                                    
                                    setup_postdata($post);
                                    
                            ?>

                            <div class="swiper-slide card" data-toggle="modal" data-target="#exampleModalCenter" data=<?php echo the_ID() ?>>

                                <?php the_post_thumbnail(); ?>

                                    <h2 class="header4"><?php the_title(); ?></h2>
                                    
                                    <?php the_excerpt(); ?>
                                    
                                    <div class="price__wrapper">
                                        <?php
                                            // проверяем есть ли в повторителе данные
                                            if( have_rows('price_weight_repeat') ):

                                                // перебираем данные
                                                while ( have_rows('price_weight_repeat') ) : the_row(); ?>
                                                <div class="card_props d-flex">

                                                    <div class="price_block">
                                                        <span class="price"> <?php the_sub_field('price'); ?> Р. </span>
                                                    </div>

                                                    <div class="weight_block">
                                                        <span class="weight"><?php the_sub_field('weight'); ?>  Г. </span>

                                                    </div> 
                                                    <?php
                                                        // отображаем вложенные поля
                                                        ?>
                                                </div>

                                                    <?php
                                                endwhile;

                                            else :

                                                // вложенных полей не найдено

                                            endif;

                                            ?>

                                        </div>

                            </div>
                            <?php
                                
                                }
                                wp_reset_postdata();  ?>
                                
                        </div>
                    </div>   
                </div>
            </div>
        </div>
    </section>


    <section class="mangal" id="mangal">
        <div class="container-fluid">

            <div class="container">

                <!-- Мангал -->
                <div class="category_title">
                    <h1 class="category_title_header">Мангал</h1>
                </div>

                <!-- Swiper Мангал -->
                <div class="strelki">   
                        <div class="swiper_arrow slider_btn_prev mangal_slider_btn_prev"><img src="<?php echo get_template_directory_uri()?>/assets/images/arrow_left.svg" alt=""></div>
                        <div class="swiper_arrow slider_btn_next mangal_slider_btn_next"><img src="<?php echo get_template_directory_uri()?>/assets/images/arrow_right.svg" alt=""></div>

                    <div class="swiper swiper_categories swiper_mangal">

                        <div class="swiper-wrapper">
                            <?php
                                global $post;
                                $postslist = get_posts( array('cat'=>'3' ) );

                                foreach ( $postslist as $post ){
                                    
                                    setup_postdata($post);
                                    
                            ?>

                            <div class="swiper-slide card" data-toggle="modal" data-target="#exampleModalCenter" data=<?php echo the_ID() ?>>

                                <?php the_post_thumbnail(); ?>

                                    <h2 class="header4"><?php the_title(); ?></h2>
                                    
                                    <?php the_excerpt(); ?>
                                    
                                    <div class="price__wrapper">
                                        <?php
                                            // проверяем есть ли в повторителе данные
                                            if( have_rows('price_weight_repeat') ):

                                                // перебираем данные
                                                while ( have_rows('price_weight_repeat') ) : the_row(); ?>
                                                <div class="card_props d-flex">

                                                    <div class="price_block">
                                                        <span class="price"> <?php the_sub_field('price'); ?> Р. </span>
                                                    </div>

                                                    <div class="weight_block">
                                                        <span class="weight"><?php the_sub_field('weight'); ?>  Г. </span>

                                                    </div> 
                                                    <?php
                                                        // отображаем вложенные поля
                                                        ?>
                                                </div>

                                                    <?php
                                                endwhile;

                                            else :

                                                // вложенных полей не найдено

                                            endif;

                                            ?>

                                        </div>

                            </div>
                            <?php
                                
                                }
                                wp_reset_postdata();  ?>
                                
                        </div>
                    </div>   
                </div>
            </div>
        </div>
    </section>

    <section class="salat" id="salat">
        <div class="container-fluid">

            <div class="container">

                <!-- Салат -->
                <div class="category_title">
                    <h1 class="category_title_header">Салат</h1>
                </div>

                <!-- Swiper Салат -->
                <div class="strelki">   

                        <div class="swiper_arrow slider_btn_prev salat_slider_btn_prev"><img src="<?php echo get_template_directory_uri()?>/assets/images/arrow_left.svg" alt=""></div>
                        <div class="swiper_arrow slider_btn_next salat_slider_btn_next"><img src="<?php echo get_template_directory_uri()?>/assets/images/arrow_right.svg" alt=""></div>

                    <div class="swiper swiper_categories swiper_salat">

                        <div class="swiper-wrapper">
                            <?php
                                global $post;
                                $postslist = get_posts( array('cat'=>'3' ) );

                                foreach ( $postslist as $post ){
                                    
                                    setup_postdata($post);
                                    
                            ?>

                            <div class="swiper-slide card" data-toggle="modal" data-target="#exampleModalCenter" data=<?php echo the_ID() ?>>

                                <?php the_post_thumbnail(); ?>

                                    <h2 class="header4"><?php the_title(); ?></h2>
                                    
                                    <?php the_excerpt(); ?>
                                    
                                    <div class="price__wrapper">
                                        <?php
                                            // проверяем есть ли в повторителе данные
                                            if( have_rows('price_weight_repeat') ):

                                                // перебираем данные
                                                while ( have_rows('price_weight_repeat') ) : the_row(); ?>
                                                <div class="card_props d-flex">

                                                    <div class="price_block">
                                                        <span class="price"> <?php the_sub_field('price'); ?> Р. </span>
                                                    </div>

                                                    <div class="weight_block">
                                                        <span class="weight"><?php the_sub_field('weight'); ?>  Г. </span>

                                                    </div> 
                                                    <?php
                                                        // отображаем вложенные поля
                                                        ?>
                                                </div>

                                                    <?php
                                                endwhile;

                                            else :

                                                // вложенных полей не найдено

                                            endif;

                                            ?>

                                    </div>

                            </div>
                            <?php
                                
                                }
                                wp_reset_postdata();  ?>
                                
                        </div>
                    </div>   
                </div>
            </div>
        </div>
    </section>


    <section class="sup" id="sup">
        <div class="container-fluid">

            <div class="container">

                <!-- Супы -->
                <div class="category_title">
                    <h1 class="category_title_header">Супы</h1>
                </div>

                <!-- Swiper Супы -->
                <div class="strelki">   

                        <div class="swiper_arrow slider_btn_prev sup_slider_btn_prev"><img src="<?php echo get_template_directory_uri()?>/assets/images/arrow_left.svg" alt=""></div>
                        <div class="swiper_arrow slider_btn_next sup_slider_btn_next"><img src="<?php echo get_template_directory_uri()?>/assets/images/arrow_right.svg" alt=""></div>

                    <div class="swiper swiper_categories swiper_sup">

                        <div class="swiper-wrapper">
                            <?php
                                global $post;
                                $postslist = get_posts( array('cat'=>'3' ) );

                                foreach ( $postslist as $post ){
                                    
                                    setup_postdata($post);
                                    
                            ?>

                            <div class="swiper-slide card" data-toggle="modal" data-target="#exampleModalCenter" data=<?php echo the_ID() ?>>

                                <?php the_post_thumbnail(); ?>

                                    <h2 class="header4"><?php the_title(); ?></h2>
                                    
                                    <?php the_excerpt(); ?>
                                    
                                    <div class="price__wrapper">
                                        <?php
                                            // проверяем есть ли в повторителе данные
                                            if( have_rows('price_weight_repeat') ):

                                                // перебираем данные
                                                while ( have_rows('price_weight_repeat') ) : the_row(); ?>
                                                <div class="card_props d-flex">

                                                    <div class="price_block">
                                                        <span class="price"> <?php the_sub_field('price'); ?> Р. </span>
                                                    </div>

                                                    <div class="weight_block">
                                                        <span class="weight"><?php the_sub_field('weight'); ?>  Г. </span>

                                                    </div> 
                                                    <?php
                                                        // отображаем вложенные поля
                                                        ?>
                                                </div>

                                                    <?php
                                                endwhile;

                                            else :

                                                // вложенных полей не найдено

                                            endif;

                                            ?>

                                    </div>

                            </div>
                            <?php
                                
                                }
                                wp_reset_postdata();  ?>
                                
                        </div>
                    </div>   
                </div>
            </div>
        </div>
    </section>

    <section class="snacks_sous" id="snacks_sous">
        <div class="container-fluid">

            <div class="container">

                <!-- Снеки и соусы-->
                <div class="category_title">
                    <h1 class="category_title_header">Снеки и соусы</h1>
                </div>

                <!-- Swiper Снеки и соусы -->
                <div class="strelki">   

                        <div class="swiper_arrow slider_btn_prev snacks_sous_slider_btn_prev"><img src="<?php echo get_template_directory_uri()?>/assets/images/arrow_left.svg" alt=""></div>
                        <div class="swiper_arrow slider_btn_next snacks_sous_slider_btn_next"><img src="<?php echo get_template_directory_uri()?>/assets/images/arrow_right.svg" alt=""></div>

                    <div class="swiper swiper_categories swiper_snacks_sous">

                        <div class="swiper-wrapper">
                            <?php
                                global $post;
                                $postslist = get_posts( array('cat'=>'3' ) );

                                foreach ( $postslist as $post ){
                                    
                                    setup_postdata($post);
                                    
                            ?>

                            <div class="swiper-slide card" data-toggle="modal" data-target="#exampleModalCenter" data=<?php echo the_ID() ?>>

                                <?php the_post_thumbnail(); ?>

                                    <h2 class="header4"><?php the_title(); ?></h2>
                                    
                                    <?php the_excerpt(); ?>
                                    
                                    <div class="price__wrapper">
                                        <?php
                                            // проверяем есть ли в повторителе данные
                                            if( have_rows('price_weight_repeat') ):

                                                // перебираем данные
                                                while ( have_rows('price_weight_repeat') ) : the_row(); ?>
                                                <div class="card_props d-flex">

                                                    <div class="price_block">
                                                        <span class="price"> <?php the_sub_field('price'); ?> Р. </span>
                                                    </div>

                                                    <div class="weight_block">
                                                        <span class="weight"><?php the_sub_field('weight'); ?>  Г. </span>

                                                    </div> 
                                                    <?php
                                                        // отображаем вложенные поля
                                                        ?>
                                                </div>

                                                    <?php
                                                endwhile;

                                            else :

                                                // вложенных полей не найдено

                                            endif;

                                            ?>

                                    </div>

                            </div>
                            <?php
                                
                                }
                                wp_reset_postdata();  ?>
                                
                        </div>
                    </div>   
                </div>
            </div>
        </div>
    </section>

    <section class="water" id="water">
        <div class="container-fluid">

            <div class="container">

                <!-- Напитки -->
                <div class="category_title">
                    <h1 class="category_title_header">Напитки</h1>
                </div>

                <!-- Swiper Напитки -->
                <div class="strelki">   

                        <div class="swiper_arrow slider_btn_prev water_slider_btn_prev"><img src="<?php echo get_template_directory_uri()?>/assets/images/arrow_left.svg" alt=""></div>
                        <div class="swiper_arrow slider_btn_next water_slider_btn_next"><img src="<?php echo get_template_directory_uri()?>/assets/images/arrow_right.svg" alt=""></div>

                    <div class="swiper swiper_categories swiper_water">

                        <div class="swiper-wrapper">
                            <?php
                                global $post;
                                $postslist = get_posts( array('cat'=>'3' ) );

                                foreach ( $postslist as $post ){
                                    
                                    setup_postdata($post);
                                    
                            ?>

                            <div class="swiper-slide card" data-toggle="modal" data-target="#exampleModalCenter" data=<?php echo the_ID() ?>>

                                <?php the_post_thumbnail(); ?>

                                    <h2 class="header4"><?php the_title(); ?></h2>
                                    
                                    <?php the_excerpt(); ?>
                                    
                                    <div class="price__wrapper">
                                        <?php
                                            // проверяем есть ли в повторителе данные
                                            if( have_rows('price_weight_repeat') ):

                                                // перебираем данные
                                                while ( have_rows('price_weight_repeat') ) : the_row(); ?>
                                                <div class="card_props d-flex">

                                                    <div class="price_block">
                                                        <span class="price"> <?php the_sub_field('price'); ?> Р. </span>
                                                    </div>

                                                    <div class="weight_block">
                                                        <span class="weight"><?php the_sub_field('weight'); ?>  Г. </span>

                                                    </div> 
                                                    <?php
                                                        // отображаем вложенные поля
                                                        ?>
                                                </div>

                                                    <?php
                                                endwhile;

                                            else :

                                                // вложенных полей не найдено

                                            endif;

                                            ?>

                                    </div>

                            </div>
                            <?php
                                
                                }
                                wp_reset_postdata();  ?>
                                
                        </div>
                    </div>   
                </div>
            </div>
        </div>
    </section>

    <section class="pahlava" id="pahlava">
        <div class="container-fluid">
            <div class="container">

                <!-- Пахлава -->
                <div class="category_title">
                    <h1 class="category_title_header">Пахлава</h1>
                </div>

                <!-- Swiper Пахлава -->
                <div class="strelki">   


                        
                        <div class="swiper_arrow slider_btn_prev pahlava_slider_btn_prev"><img src="<?php echo get_template_directory_uri()?>/assets/images/arrow_left.svg" alt=""></div>
                        <div class="swiper_arrow slider_btn_next pahlava_slider_btn_next"><img src="<?php echo get_template_directory_uri()?>/assets/images/arrow_right.svg"></div>

                    <div class="swiper swiper_categories swiper_pahlava ">

                        <div class="swiper-wrapper">
                            <?php
                                global $post;
                                $postslist = get_posts( array('cat'=>'3' ) );

                                foreach ( $postslist as $post ){
                                    
                                    setup_postdata($post);
                                    
                            ?>

                            <div class="swiper-slide card" data-toggle="modal" data-target="#exampleModalCenter" data=<?php echo the_ID() ?>>

                                <?php the_post_thumbnail(); ?>

                                    <h2 class="header4"><?php the_title(); ?></h2>
                                    
                                    <?php the_excerpt(); ?>
                                    
                                    <div class="price__wrapper">
                                        <?php
                                            // проверяем есть ли в повторителе данные
                                            if( have_rows('price_weight_repeat') ):

                                                // перебираем данные
                                                while ( have_rows('price_weight_repeat') ) : the_row(); ?>
                                                <div class="card_props d-flex">

                                                    <div class="price_block">
                                                        <span class="price"> <?php the_sub_field('price'); ?> Р. </span>
                                                    </div>

                                                    <div class="weight_block">
                                                        <span class="weight"><?php the_sub_field('weight'); ?>  Г. </span>

                                                    </div> 
                                                    <?php
                                                        // отображаем вложенные поля
                                                        ?>
                                                </div>

                                                    <?php
                                                endwhile;

                                            else :

                                                // вложенных полей не найдено

                                            endif;

                                            ?>

                                    </div>

                            </div>
                            <?php
                                
                                }
                                wp_reset_postdata();  ?>
                                
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </section>


    <section class="location">
        <div class="container-fluid black">
            <div class="container">
            <h1 class="text-center title_map">Как нас найти</h1>
            <div class="map_big"></div>
                <div class="row contact_maps">
                    <div class="col-4">
                        <p class="nomer">Петергофское шоссе 51е</p>
                        <p class="text">Напротив главного (центрального) входа в ТЦ «Жемчужная Плаза»</p>
                    </div>
                    <div class="col-4 text-center">
                        <p class="nomer">contact@beefycoin.ru</p>
                        <p class="text">Отдел по очень важным делам</p>
                        <p class="nomer">contact@beefycoin.ru</p>
                        <p class="text">Отдел по очень важным делам</p>
                    </div>
                    <div class="col-4 text-right">
                        <p class="nomer">+7 (951) 996-22-99</p>
                        <p class="text">Отдел контроля качества</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="rate">
        <div class="container rating">
           
    
        <div class="row">
            <div class="col-sm-12 col-lg-6">
                <div class="rating_title">
                    <span>Последние отзывы </span> 
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-lg-4">
                <p class="rating_title1">Почитайте, что говорят наши гости, и приезжайте скорее к нам!</p>   
            </div>
        </div>



        <div class="row rating_write">
            <div class="col-sm-12 col-lg-6 rating_write_item">
                <div class="row">
                    <div class="col-sm-12 col-lg-4">
                    <img src="<?php echo get_template_directory_uri()?>/assets/images/rez1.png">
                        
                    </div>

                    <div class="col-sm-12 col-lg-8">
                        <div class="rating_text align-items-center" >
                            <span>Елена Звягинцева</span>    
                            <img src="<?php echo get_template_directory_uri()?>/assets/images/stars.png">

                        </div>
                        
                        <p class="rating_text_recense">Безумно вкусно, классическая шаверма и куриный шашлык, сочно, ароматно, шашлык в лаваше, 2 соуса, всё посыпали гранатом, отдельно лучок с огурчиками, помидорками и зеленью. Нам так вкусно давно не было. Однозначно рекомендую! Успехов Вам и благодарных посетителей!</p>
                        <p class="rating_text_recense">Источник: 2ГИС</p>


                    </div>

                </div>

            </div>

            <div class="col-sm-12 col-lg-6 rating_write_item">
                <div class="row">
                    <div class="col-sm-12 col-lg-4">
                    <img src="<?php echo get_template_directory_uri()?>/assets/images/rez1.png">
                        
                    </div>

                    <div class="col-sm-12 col-lg-8">
                        <div class="rating_text align-items-center" >
                            <span>Елена Звягинцева</span>    
                            <img src="<?php echo get_template_directory_uri()?>/assets/images/stars.png">
                        </div>
                        
                        <p class="rating_text_recense">Нравится.Приветливый мужчина на кассе,шаверма всегда вкусная.Очень нравится лаваш в который заворачивают.Грязи не заметила,как писали в другом отзыве.Меня все устроило,зайду ещё обязательно!</p>
                        <p class="rating_text_recense">Источник: 2ГИС</p>


                    </div>

                </div>

            </div>
        </div>

        <div class="row rating_write">
            <div class="col-sm-12 col-lg-6 rating_write_item">
                <div class="row">
                    <div class="col-sm-12 col-lg-4">
                    <img src="<?php echo get_template_directory_uri()?>/assets/images/rez1.png">
                        
                    </div>

                    <div class="col-sm-12 col-lg-8">
                        <div class="rating_text align-items-center" >
                            <span>Елена Звягинцева</span>    
                            <img src="<?php echo get_template_directory_uri()?>/assets/images/stars.png">

                        </div>
                        
                        <p class="rating_text_recense">Если честно не ожидал такого сервиса качества как получил в этом замечательном месте. Заказал куриный шашлык за 300 руб принесли вовремя пришёл домой открыл а к шашлыку отдельно нарезан огурчик помидорка лучок и всё это посыпано свежим укропом и петрушкой. Шашлычек с мягким,вкусным лавашем и соусом. я был просто в шоке очень вкусно очень свежее всё. И упаковано было нереально хорошо. Правда не часто встретишь настолько вкусно сделано с душой всё красиво как полагается Я теперь у вас постоянно клиент замечательное место рекомендую!!!</p>
                        <p class="rating_text_recense">Источник: 2ГИС</p>


                    </div>

                </div>

            </div>

            <div class="col-sm-12 col-lg-6 rating_write_item">
                <div class="row">
                    <div class="col-sm-12 col-lg-4">
                    <img src="<?php echo get_template_directory_uri()?>/assets/images/rez1.png">
                        
                    </div>

                    <div class="col-sm-12 col-lg-8">
                        <div class="rating_text align-items-center" >
                            <span>Елена Звягинцева</span>    
                            <img src="<?php echo get_template_directory_uri()?>/assets/images/stars.png">
                        </div>
                        
                        <p class="rating_text_recense">Осталась очень довольна данным заведением. Шаверма очень понравилась. Продукты свежие. Шаверма вкусная, сытная и ооччеенньь много начинки внутри. Приветливые сотрудники и приятный интерьер. Осталась в восторге, благодарю за ваш труд, обязательно вернусь ещё 😍</p>
                        <p class="rating_text_recense">Источник: 2ГИС</p>


                    </div>

                </div>

            </div>
        </div>
    </section>

    <section class="contact_form">

    <div class="container-fluid">
        <img class="background_image" src="<?php echo get_template_directory_uri()?>/assets/images/back_contact_form.png" alt="">

        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-12">
                    <form class="forma_svyazy" action="/mail.php" method="post">
                    <h1 class="text-center title_map_form">Отправьте свое резюме</h1>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Имя <span class="red">*</span></label>
                            <input type="text" name="name" class="form-control" id="inputEmail4">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Телефон <span class="red">*</span></label>
                            <input type="tel" name="phone" class="form-control" id="inputPassword4" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAddress">Почта <span class="red">*</span></label>
                        <input type="email" name="email" class="form-control" id="inputAddress">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Комментарий</label>
                        <textarea class="form-control" name="message" rows="3"></textarea>
                    </div>
                    <button type="submit" class="button_submit_form">ОТПРАВИТЬ</button>
                    <p class="text_under_submit_form">Нажимая на кнопку "Отправить" вы принимаете политику конфиденциальности</p>
                </form>
                </div>
            </div>
        </div>
    </div>
    </section>




    

</div>

<footer class="black">
    <div class="container-fluid black">
        <div class="row block_1">
            <div class="col-6 col-lg-3 logo_col"><img src="<?php echo get_template_directory_uri()?>/assets/images/Logo.png">
                <div class="text-time d-none d-sm-block">Ежедневно 10:00 - 22:00</div>
            </div>
            <div class="col-3 d-none d-sm-flex"><?php wp_nav_menu() ?></div>
            <div class="col-3 d-none d-sm-flex ikonki">
                <div class="rabota"></div>
                <img src="<?php echo get_template_directory_uri()?>/assets/images/icon_shava.png">
                <img src="<?php echo get_template_directory_uri()?>/assets/images/insta.svg">
                <img src="<?php echo get_template_directory_uri()?>/assets/images/vk.svg"> </div>
            <div class="col-3 d-none d-sm-block telefon"><p class="nomer">+7 (951) 996-22-99</p><p class="text">Отдел контроля качества</p></div>
            <div class="col-6  d-sm-block d-md-none d-lg-none">
                <nav class="navbar">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span><img class="icon_top_head" src="<?php echo get_template_directory_uri(); ?>/assets/images/burger.png"></span>
                    </button>
                </nav>
            </div>
        </div>
        <div class="">
            <div class="collapse" id="navbarToggleExternalContent">
                <div class="mobile_menu">
                    <h4 class="text-white"></h4>
                    <?php wp_nav_menu() ?>
                    <div class="corch">
                    <div class="ikonki">
                    <div class="rabota"></div>
                    <img src="<?php echo get_template_directory_uri()?>/assets/images/icon_shava.png">
                    <img src="<?php echo get_template_directory_uri()?>/assets/images/insta.svg">
                    <img src="<?php echo get_template_directory_uri()?>/assets/images/vk.svg">
                    </div>
                    <div class="text-time">Режим работы заведения:</div>
                    <div class="text-time">Ежедневно 10:00 - 22:00</div>
                    <div class="telefon"><p class="nomer">+7 (951) 996-22-99</p><p class="text">Отдел контроля качества</p></div>
                        <div class="text-time">ИНН 890412690088 <br>
                            ОГРН 321784700226547
                        </div>
                        <div class="text-time">© 2022 Beefy Coin</div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    
    <!-- HEader modal -->
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="rabota_text">
                <div class="text_rabota">
                    В нашу команду BeefyCoin требуется девушка на <b>вакансию продавец</b> со сменным графиком работы. Консультант, который станет другом для наших гостей и сможет найти подход к каждому!

                Мы недавно открыли островок в Санкт-Петербурге в торговом центре «Жемчужная плаза», так же у нас уже есть стрит фуд кафе.

                Наш магазин предлагает различные турецкие сладости и пахлаву собственного производства.
                </div>
                    <h2>Обязанности:</h2>
                - Консультирование покупателей по товарному ассортименту
                - Раскладка товара и контроль остатков
                - Работа с кассой
                - Поддержание порядка и чистоты в магазине
                - Открытие и закрытие смены и магазина

                    <h2>Требования:</h2>
                - Честность, ответственность и пунктуальность
                - Опыт работы в продажах и умение презентовать товар

                    <h2>Мы предлагаем:</h2>
                - Оклад
                - Процент от продаж %
                - Сменный график (можно брать больше смен по договоренности) с 10:00 до 22:00
                - Точка расположена в ТЦ «Жемчужная плаза» (ближайшее метро Ленинский, Ветеранов)
                </div>
                <div class="container params">
                <form class="forma_svyazy" action="/mail.php" method="post">
                    <h1 class="text-center title_map_form">Отправьте свое резюме</h1>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Имя <span class="red">*</span></label>
                            <input type="text" name="name" class="form-control" id="inputEmail4">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">Телефон <span class="red">*</span></label>
                            <input type="tel" name="phone" class="form-control" id="inputPassword4" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAddress">Почта <span class="red">*</span></label>
                        <input type="email" name="email" class="form-control" id="inputAddress">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Комментарий</label>
                        <textarea class="form-control" name="message" rows="3"></textarea>
                    </div>
                    <button type="submit" class="button_submit_form">ОТПРАВИТЬ</button>
                    <p class="text_under_submit_form">Нажимая на кнопку "Отправить" вы принимаете политику конфиденциальности</p>
                </form>
                </div>
            </div>
        </div>
    </div>
</footer>