<?php
/* Template Name: Landing-page */
?>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <?php wp_head(); ?>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>

</head>
<header>
    <div class="container">
        <div class="row block_1">
            <div class="col-3 logo_col"><img src="<?php echo get_template_directory_uri()?>/assets/images/Logo.png">
            <div class="text-time">Ежедневно 10:00 - 22:00</div>
            </div>
            <div class="col-3"><?php wp_nav_menu() ?></div>
            <div class="col-3">s</div>
            <div class="col-3">s</div>
        </div>
    </div>
</header>